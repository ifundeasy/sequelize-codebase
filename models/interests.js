const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'interests',
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      interestType: {
        type: DataTypes.ENUM('experience', 'interest'),
        allowNull: false,
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      commodityId: {
        type: DataTypes.TINYINT,
        allowNull: true,
        references: {
          model: 'commodities',
          key: 'id',
        },
      },
      commodityTypeId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'commodityTypes',
          key: 'id',
        },
      },
      farmingExperience: {
        type: DataTypes.INTEGER.UNSIGNED.ZEROFILL,
        allowNull: false,
        defaultValue: 00000000,
        comment: 'Pengalaman bertani',
      },
      isActive: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1,
      },
      createdBy: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      notes: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
    },
    {
      sequelize,
      tableName: 'interests',
      timestamps: false,
      underscored: false,
      freezeTableName: true,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'id' }],
        },
        {
          name: 'commodityId',
          using: 'BTREE',
          fields: [{ name: 'commodityId' }],
        },
        {
          name: 'commodityTypeId',
          using: 'BTREE',
          fields: [{ name: 'commodityTypeId' }],
        },
      ],
    }
  );
};
