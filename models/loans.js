const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'loans',
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: 'name',
      },
      cultivationCons: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: 'cultivationCons',
      },
      cultivationPros: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: 'cultivationPros',
      },
      brokerId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      customerId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      isActive: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1,
      },
      createdBy: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      notes: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
    },
    {
      sequelize,
      tableName: 'loans',
      timestamps: false,
      underscored: false,
      freezeTableName: true,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'id' }],
        },
        {
          name: 'name',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'name' }],
        },
        {
          name: 'cultivationCons',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'cultivationCons' }],
        },
        {
          name: 'cultivationPros',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'cultivationPros' }],
        },
      ],
    }
  );
};
