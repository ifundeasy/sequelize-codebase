const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'reminders',
    {
      id: {
        autoIncrement: true,
        type: DataTypes.TINYINT,
        allowNull: false,
        primaryKey: true,
      },
      agentType: {
        type: DataTypes.ENUM('sms', 'email', 'whatsapp', 'telephone', 'notification'),
        allowNull: false,
      },
      dueValue: {
        type: DataTypes.INTEGER.UNSIGNED.ZEROFILL,
        allowNull: false,
      },
      dueTimeUnit: {
        type: DataTypes.ENUM('day', 'week', 'month', 'year'),
        allowNull: false,
      },
      recurrentValue: {
        type: DataTypes.INTEGER.UNSIGNED.ZEROFILL,
        allowNull: false,
      },
      recurrentTimeUnit: {
        type: DataTypes.ENUM('day', 'week', 'month', 'year'),
        allowNull: true,
      },
      inTime: {
        type: DataTypes.TIME,
        allowNull: false,
      },
      isActive: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1,
      },
      createdBy: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      notes: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
    },
    {
      sequelize,
      tableName: 'reminders',
      timestamps: false,
      underscored: false,
      freezeTableName: true,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'id' }],
        },
      ],
    }
  );
};
