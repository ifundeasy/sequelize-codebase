const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'cartItems',
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      cartId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'carts',
          key: 'id',
        },
      },
      itemName: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      nominal: {
        type: DataTypes.INTEGER.UNSIGNED.ZEROFILL,
        allowNull: true,
      },
      nominalAccepted: {
        type: DataTypes.INTEGER.UNSIGNED.ZEROFILL,
        allowNull: true,
      },
      itemDesc: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      verifyStatus: {
        type: DataTypes.ENUM('pending', 'accepted', 'refused'),
        allowNull: false,
      },
      isActive: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1,
      },
      createdBy: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      notes: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
    },
    {
      sequelize,
      tableName: 'cartItems',
      timestamps: false,
      underscored: false,
      freezeTableName: true,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'id' }],
        },
        {
          name: 'cartId',
          using: 'BTREE',
          fields: [{ name: 'cartId' }],
        },
      ],
    }
  );
};
