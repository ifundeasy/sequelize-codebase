const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'messages',
    {
      id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      subjectId: {
        type: DataTypes.TINYINT,
        allowNull: true,
        references: {
          model: 'subjects',
          key: 'id',
        },
      },
      agentType: {
        type: DataTypes.ENUM('sms', 'email', 'whatsapp', 'telephone', 'notification'),
        allowNull: false,
      },
      sender: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      receiver: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      secret: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      content: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      maxAge: {
        type: DataTypes.INTEGER,
        allowNull: false,
        comment: 'Masa berlaku, dalam detik',
      },
      isActive: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1,
      },
      createdBy: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      notes: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
    },
    {
      sequelize,
      tableName: 'messages',
      timestamps: false,
      underscored: false,
      freezeTableName: true,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'id' }],
        },
        {
          name: 'subjectId',
          using: 'BTREE',
          fields: [{ name: 'subjectId' }],
        },
      ],
    }
  );
};
