const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'users',
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      registrantId: {
        type: DataTypes.BIGINT,
        allowNull: true,
        references: {
          model: 'registrants',
          key: 'id',
        },
      },
      email: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: 'email',
      },
      phone: {
        type: DataTypes.STRING(14),
        allowNull: false,
      },
      username: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: 'username',
      },
      password: {
        type: DataTypes.STRING(60),
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      nik: {
        type: DataTypes.STRING(16),
        allowNull: false,
        comment: 'Nomor KTP',
        unique: 'nik',
      },
      accountNum: {
        type: DataTypes.STRING(16),
        allowNull: true,
        comment: 'Nomor rekening',
      },
      placeOfBirth: {
        type: DataTypes.STRING(30),
        allowNull: true,
        defaultValue: 'NULL',
      },
      dateOfBirth: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      gender: {
        type: DataTypes.TINYINT,
        allowNull: true,
      },
      shopName: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'NULL',
      },
      originAddr1: {
        type: DataTypes.STRING(100),
        allowNull: true,
        defaultValue: 'NULL',
      },
      originAddr2: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
      currentAddr1: {
        type: DataTypes.STRING(100),
        allowNull: true,
        defaultValue: 'NULL',
      },
      currentAddr2: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
      maritalStatus: {
        type: DataTypes.ENUM('single', 'merried', 'widow'),
        allowNull: true,
      },
      numOfDependents: {
        type: DataTypes.TINYINT.UNSIGNED.ZEROFILL,
        allowNull: false,
        defaultValue: 0000,
      },
      nikImg: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
        comment: 'Foto KTP',
      },
      nikSelfieImg: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
        comment: 'Foto selfie dengan memegang KTP',
      },
      kkImg: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
        comment: 'Foto KK',
      },
      landDeedImg: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
        comment: 'Foto akte lahan pertanian',
      },
      verifyStatus: {
        type: DataTypes.ENUM('pending', 'accepted', 'refused'),
        allowNull: false,
      },
      isActive: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1,
      },
      createdBy: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      notes: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
    },
    {
      sequelize,
      tableName: 'users',
      timestamps: false,
      underscored: false,
      freezeTableName: true,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'id' }],
        },
        {
          name: 'email',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'email' }],
        },
        {
          name: 'username',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'username' }],
        },
        {
          name: 'nik',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'nik' }],
        },
        {
          name: 'registrantId',
          using: 'BTREE',
          fields: [{ name: 'registrantId' }],
        },
        {
          name: 'users_nik_email_username',
          using: 'BTREE',
          fields: [{ name: 'nik' }, { name: 'email' }, { name: 'username' }],
        },
      ],
    }
  );
};
