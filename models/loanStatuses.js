const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'loanStatuses',
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      loanId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'loans',
          key: 'id',
        },
      },
      tenorId: {
        type: DataTypes.TINYINT,
        allowNull: true,
        references: {
          model: 'tenors',
          key: 'id',
        },
      },
      installmentId: {
        type: DataTypes.TINYINT,
        allowNull: true,
        references: {
          model: 'installments',
          key: 'id',
        },
      },
      verifyStatus: {
        type: DataTypes.ENUM('pending', 'accepted', 'refused'),
        allowNull: false,
      },
      startDate: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      endDate: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      isActive: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1,
      },
      createdBy: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: DataTypes.STRING(50),
        allowNull: true,
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      notes: {
        type: DataTypes.STRING(255),
        allowNull: true,
        defaultValue: 'NULL',
      },
    },
    {
      sequelize,
      tableName: 'loanStatuses',
      timestamps: false,
      underscored: false,
      freezeTableName: true,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [{ name: 'id' }],
        },
        {
          name: 'loanId',
          using: 'BTREE',
          fields: [{ name: 'loanId' }],
        },
        {
          name: 'tenorId',
          using: 'BTREE',
          fields: [{ name: 'tenorId' }],
        },
        {
          name: 'installmentId',
          using: 'BTREE',
          fields: [{ name: 'installmentId' }],
        },
      ],
    }
  );
};
