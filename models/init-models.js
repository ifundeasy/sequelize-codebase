var DataTypes = require('sequelize').DataTypes;
var _agricultures = require('./agricultures');
var _cartCheckouts = require('./cartCheckouts');
var _cartItems = require('./cartItems');
var _carts = require('./carts');
var _commodities = require('./commodities');
var _commodityTypes = require('./commodityTypes');
var _fundTypes = require('./fundTypes');
var _groupMembers = require('./groupMembers');
var _groups = require('./groups');
var _installments = require('./installments');
var _interests = require('./interests');
var _loanItems = require('./loanItems');
var _loanReminders = require('./loanReminders');
var _loanStatuses = require('./loanStatuses');
var _loans = require('./loans');
var _messages = require('./messages');
var _oAuths = require('./oAuths');
var _registrants = require('./registrants');
var _reminders = require('./reminders');
var _roles = require('./roles');
var _sessions = require('./sessions');
var _subjects = require('./subjects');
var _tenors = require('./tenors');
var _users = require('./users');

function initModels(sequelize) {
  var agricultures = _agricultures(sequelize, DataTypes);
  var cartCheckouts = _cartCheckouts(sequelize, DataTypes);
  var cartItems = _cartItems(sequelize, DataTypes);
  var carts = _carts(sequelize, DataTypes);
  var commodities = _commodities(sequelize, DataTypes);
  var commodityTypes = _commodityTypes(sequelize, DataTypes);
  var fundTypes = _fundTypes(sequelize, DataTypes);
  var groupMembers = _groupMembers(sequelize, DataTypes);
  var groups = _groups(sequelize, DataTypes);
  var installments = _installments(sequelize, DataTypes);
  var interests = _interests(sequelize, DataTypes);
  var loanItems = _loanItems(sequelize, DataTypes);
  var loanReminders = _loanReminders(sequelize, DataTypes);
  var loanStatuses = _loanStatuses(sequelize, DataTypes);
  var loans = _loans(sequelize, DataTypes);
  var messages = _messages(sequelize, DataTypes);
  var oAuths = _oAuths(sequelize, DataTypes);
  var registrants = _registrants(sequelize, DataTypes);
  var reminders = _reminders(sequelize, DataTypes);
  var roles = _roles(sequelize, DataTypes);
  var sessions = _sessions(sequelize, DataTypes);
  var subjects = _subjects(sequelize, DataTypes);
  var tenors = _tenors(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);

  cartCheckouts.belongsTo(carts, { as: 'cart', foreignKey: 'cartId' });
  carts.hasMany(cartCheckouts, { as: 'cartCheckouts', foreignKey: 'cartId' });
  cartItems.belongsTo(carts, { as: 'cart', foreignKey: 'cartId' });
  carts.hasMany(cartItems, { as: 'cartItems', foreignKey: 'cartId' });
  commodityTypes.belongsTo(commodities, { as: 'commodity', foreignKey: 'commodityId' });
  commodities.hasMany(commodityTypes, { as: 'commodityTypes', foreignKey: 'commodityId' });
  groups.belongsTo(commodities, { as: 'commodity', foreignKey: 'commodityId' });
  commodities.hasMany(groups, { as: 'groups', foreignKey: 'commodityId' });
  interests.belongsTo(commodities, { as: 'commodity', foreignKey: 'commodityId' });
  commodities.hasMany(interests, { as: 'interests', foreignKey: 'commodityId' });
  groups.belongsTo(commodityTypes, { as: 'commodityType', foreignKey: 'commodityTypeId' });
  commodityTypes.hasMany(groups, { as: 'groups', foreignKey: 'commodityTypeId' });
  interests.belongsTo(commodityTypes, { as: 'commodityType', foreignKey: 'commodityTypeId' });
  commodityTypes.hasMany(interests, { as: 'interests', foreignKey: 'commodityTypeId' });
  loanItems.belongsTo(fundTypes, { as: 'fundType', foreignKey: 'fundTypeId' });
  fundTypes.hasMany(loanItems, { as: 'loanItems', foreignKey: 'fundTypeId' });
  groupMembers.belongsTo(groups, { as: 'group', foreignKey: 'groupId' });
  groups.hasMany(groupMembers, { as: 'groupMembers', foreignKey: 'groupId' });
  loanStatuses.belongsTo(installments, { as: 'installment', foreignKey: 'installmentId' });
  installments.hasMany(loanStatuses, { as: 'loanStatuses', foreignKey: 'installmentId' });
  loanItems.belongsTo(loans, { as: 'loan', foreignKey: 'loanId' });
  loans.hasMany(loanItems, { as: 'loanItems', foreignKey: 'loanId' });
  loanReminders.belongsTo(loans, { as: 'loan', foreignKey: 'loanId' });
  loans.hasMany(loanReminders, { as: 'loanReminders', foreignKey: 'loanId' });
  loanStatuses.belongsTo(loans, { as: 'loan', foreignKey: 'loanId' });
  loans.hasMany(loanStatuses, { as: 'loanStatuses', foreignKey: 'loanId' });
  users.belongsTo(registrants, { as: 'registrant', foreignKey: 'registrantId' });
  registrants.hasMany(users, { as: 'users', foreignKey: 'registrantId' });
  messages.belongsTo(subjects, { as: 'subject', foreignKey: 'subjectId' });
  subjects.hasMany(messages, { as: 'messages', foreignKey: 'subjectId' });
  loanStatuses.belongsTo(tenors, { as: 'tenor', foreignKey: 'tenorId' });
  tenors.hasMany(loanStatuses, { as: 'loanStatuses', foreignKey: 'tenorId' });
  oAuths.belongsTo(users, { as: 'user', foreignKey: 'userId' });
  users.hasMany(oAuths, { as: 'oAuths', foreignKey: 'userId' });
  roles.belongsTo(users, { as: 'user', foreignKey: 'userId' });
  users.hasMany(roles, { as: 'roles', foreignKey: 'userId' });
  sessions.belongsTo(users, { as: 'user', foreignKey: 'userId' });
  users.hasMany(sessions, { as: 'sessions', foreignKey: 'userId' });

  return {
    agricultures,
    cartCheckouts,
    cartItems,
    carts,
    commodities,
    commodityTypes,
    fundTypes,
    groupMembers,
    groups,
    installments,
    interests,
    loanItems,
    loanReminders,
    loanStatuses,
    loans,
    messages,
    oAuths,
    registrants,
    reminders,
    roles,
    sessions,
    subjects,
    tenors,
    users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
