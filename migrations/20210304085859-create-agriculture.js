'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'agricultures',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        userId: {
          type: Sequelize.INTEGER
        },
        latitude: {
          allowNull: false,
          type: Sequelize.DECIMAL(10, 8),
        },
        longitude: {
          allowNull: false,
          type: Sequelize.DECIMAL(11, 8),
        },
        northFaceImg: {
          allowNull: false,
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
        eastFaceImg: {
          allowNull: false,
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
        southFaceImg: {
          allowNull: false,
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
        westFaceImg: {
          allowNull: false,
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      {
        comment: 'Lahan pertanian',
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('agricultures');
  },
};
