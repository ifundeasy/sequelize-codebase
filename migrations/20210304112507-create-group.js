'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('groups', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(8),
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(100),
        unique: true,
      },
      commodityId: {
        type: Sequelize.TINYINT(4),
        references: {
          model: 'commodities',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      commodityTypeId: {
        type: Sequelize.INTEGER(8),
        references: {
          model: 'commodityTypes',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      isActive: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      notes: {
        type: Sequelize.STRING,
        defaultValue: 'NULL',
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('groups');
  },
};
