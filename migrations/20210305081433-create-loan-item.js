'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('loanItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      loanId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'loans',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      fundTypeId: {
        type: Sequelize.TINYINT(4),
        references: {
          model: 'fundTypes',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      itemName: {
        type: Sequelize.STRING(100),
      },
      nominal: {
        type: Sequelize.INTEGER.ZEROFILL.UNSIGNED,
      },
      nominalAccepted: {
        type: Sequelize.INTEGER.ZEROFILL.UNSIGNED,
      },
      itemDesc: {
        type: Sequelize.TEXT,
      },
      verifyStatus: {
        allowNull: false,
        type: Sequelize.ENUM(Object.values(Enum.verifyStatus)),
      },
      isActive: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      notes: {
        type: Sequelize.STRING,
        defaultValue: 'NULL',
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('loanItems');
  }
};