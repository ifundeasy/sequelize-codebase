'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('groupMembers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      groupId: {
        type: Sequelize.INTEGER(8),
        references: {
          model: 'groups',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      userId: {
        type: Sequelize.INTEGER
      },
      memberType: {
        allowNull: false,
        type: Sequelize.ENUM(Object.values(Enum.memberType)),
      },
      isActive: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      notes: {
        type: Sequelize.STRING,
        defaultValue: 'NULL',
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('groupMembers');
  },
};
