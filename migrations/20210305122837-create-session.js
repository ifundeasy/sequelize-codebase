'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('sessions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT,
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      token: {
        allowNull: false,
        type: Sequelize.STRING(100),
        unique: true,
      },
      maxAge: {
        allowNull: false,
        type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
        comment: 'Masa berlaku, dalam detik',
      },
      lastUsage: {
        type: Sequelize.DATE,
      },
      isActive: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      notes: {
        type: Sequelize.STRING,
        defaultValue: 'NULL',
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('sessions');
  }
};