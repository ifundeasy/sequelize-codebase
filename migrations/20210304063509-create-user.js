'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable(
        'users',
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          registrantId: {
            type: Sequelize.BIGINT,
            references: {
              model: 'registrants',
              key: 'id',
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL',
          },
          email: {
            allowNull: false,
            type: Sequelize.STRING(50),
            unique: true,
          },
          phone: {
            allowNull: false,
            type: Sequelize.STRING(14),
          },
          username: {
            allowNull: false,
            type: Sequelize.STRING(50),
            unique: true,
          },
          password: {
            allowNull: false,
            type: Sequelize.STRING(60),
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING(100),
          },
          nik: {
            allowNull: false,
            type: Sequelize.STRING(16),
            unique: true,
            comment: 'Nomor KTP',
          },
          accountNum: {
            type: Sequelize.STRING(16),
            comment: 'Nomor rekening',
          },
          placeOfBirth: {
            type: Sequelize.STRING(30),
            defaultValue: 'NULL',
          },
          dateOfBirth: {
            type: Sequelize.DATEONLY,
          },
          gender: {
            type: Sequelize.BOOLEAN,
          },
          shopName: {
            type: Sequelize.STRING(50),
            defaultValue: 'NULL',
          },
          originAddr1: {
            type: Sequelize.STRING(100),
            defaultValue: 'NULL',
          },
          originAddr2: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
          },
          currentAddr1: {
            type: Sequelize.STRING(100),
            defaultValue: 'NULL',
          },
          currentAddr2: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
          },
          maritalStatus: {
            type: Sequelize.ENUM(Object.values(Enum.maritalStatus)),
          },
          numOfDependents: {
            allowNull: false,
            type: Sequelize.TINYINT(4).ZEROFILL.UNSIGNED,
            defaultValue: 0,
          },
          nikImg: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
            comment: 'Foto KTP',
          },
          nikSelfieImg: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
            comment: 'Foto selfie dengan memegang KTP',
          },
          kkImg: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
            comment: 'Foto KK',
          },
          landDeedImg: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
            comment: 'Foto akte lahan pertanian',
          },
          verifyStatus: {
            allowNull: false,
            type: Sequelize.ENUM(Object.values(Enum.verifyStatus)),
          },
          isActive: {
            allowNull: false,
            type: Sequelize.BOOLEAN,
            defaultValue: true,
          },
          createdBy: {
            allowNull: false,
            type: Sequelize.STRING(50),
            defaultValue: 'SYSTEM',
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
          },
          updatedBy: {
            type: Sequelize.STRING(50),
            defaultValue: 'SYSTEM',
          },
          updatedAt: {
            type: Sequelize.DATE,
          },
          notes: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('users', ['nik', 'email', 'username'], {
        indicesType: 'UNIQUE',
        transaction,
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  },
};
