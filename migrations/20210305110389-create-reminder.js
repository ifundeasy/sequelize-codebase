'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'reminders',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.TINYINT(4),
        },
        agentType: {
          allowNull: false,
          type: Sequelize.ENUM(Object.values(Enum.agentType)),
        },
        dueValue: {
          allowNull: false,
          type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
        },
        dueTimeUnit: {
          allowNull: false,
          type: Sequelize.ENUM(Object.keys(Enum.timeUnitType)),
        },
        recurrentValue: {
          allowNull: false,
          type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
        },
        recurrentTimeUnit: {
          type: Sequelize.ENUM(Object.keys(Enum.timeUnitType)),
        },
        inTime: {
          allowNull: false,
          type: Sequelize.TIME,
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      { comment: '[MASTER] Template reminder, pengingat jatuh tempo customer' }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('reminders');
  }
};