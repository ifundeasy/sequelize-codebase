'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'interests',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        interestType: {
          allowNull: false,
          type: Sequelize.ENUM(Object.values(Enum.interestType)),
        },
        userId: {
          type: Sequelize.INTEGER
        },
        commodityId: {
          type: Sequelize.TINYINT(4),
          references: {
            model: 'commodities',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        commodityTypeId: {
          type: Sequelize.INTEGER(8),
          references: {
            model: 'commodityTypes',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        farmingExperience: {
          allowNull: false,
          type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
          defaultValue: 0,
          comment: 'Pengalaman bertani',
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      {
        comment: 'Riwayat dan minat bertani',
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('interests');
  },
};
