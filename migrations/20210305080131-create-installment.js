'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'installments',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.TINYINT(4),
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING(100),
          unique: true,
        },
        recurrentValue: {
          allowNull: false,
          type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
        },
        recurrentTimeUnit: {
          allowNull: false,
          type: Sequelize.ENUM(Object.keys(Enum.timeUnitType)),
        },
        rate: {
          allowNull: false,
          type: Sequelize.INTEGER.ZEROFILL.UNSIGNED,
          defaultValue: 0,
        },
        rateType: {
          allowNull: false,
          type: Sequelize.ENUM(Object.keys(Enum.rateType)),
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      { comment: '[MASTER] Angsuran' }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('installments');
  }
};