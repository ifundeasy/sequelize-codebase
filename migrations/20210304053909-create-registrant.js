'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable(
        'registrants',
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.BIGINT,
          },
          name: {
            allowNull: true,
            type: Sequelize.STRING(100),
            unique: true,
          },
          email: {
            allowNull: false,
            type: Sequelize.STRING(50),
            unique: true,
          },
          password: {
            allowNull: false,
            type: Sequelize.STRING(60),
          },
          messageId: {
            type: Sequelize.BIGINT
          },
          verifyStatus: {
            allowNull: false,
            type: Sequelize.ENUM(Object.values(Enum.verifyStatus)),
          },
          isActive: {
            allowNull: false,
            type: Sequelize.BOOLEAN,
            defaultValue: true,
          },
          createdBy: {
            allowNull: false,
            type: Sequelize.STRING(50),
            defaultValue: 'SYSTEM',
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
          },
          updatedBy: {
            type: Sequelize.STRING(50),
            defaultValue: 'SYSTEM',
          },
          updatedAt: {
            type: Sequelize.DATE,
          },
          notes: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
          },
        },
        {
          transaction,
          comment: 'Tabel untuk mengampung registrasi user baru',
        }
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('registrants');
  },
};
