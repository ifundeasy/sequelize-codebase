'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'commodityTypes',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER(8),
        },
        commodityId: {
          type: Sequelize.TINYINT(4),
          references: {
            model: 'commodities',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true,
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      { comment: '[MASTER] Jenis tanaman dari suatu komoditas' }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('commodityTypes');
  },
};
