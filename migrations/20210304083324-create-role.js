'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable(
        'roles',
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          userId: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id',
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL',
          },
          roleType: {
            allowNull: false,
            type: Sequelize.ENUM(Object.values(Enum.roleType)),
          },
          isActive: {
            allowNull: false,
            type: Sequelize.BOOLEAN,
            defaultValue: true,
          },
          createdBy: {
            allowNull: false,
            type: Sequelize.STRING(50),
            defaultValue: 'SYSTEM',
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
          },
          updatedBy: {
            type: Sequelize.STRING(50),
            defaultValue: 'SYSTEM',
          },
          updatedAt: {
            type: Sequelize.DATE,
          },
          notes: {
            type: Sequelize.STRING,
            defaultValue: 'NULL',
          },
        },
        { transaction }
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('roles');
  },
};
