'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'tenors',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.TINYINT(4),
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING(100),
          unique: true,
        },
        value: {
          allowNull: false,
          type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
        },
        timeUnitType: {
          allowNull: false,
          type: Sequelize.ENUM(Object.keys(Enum.timeUnitType)),
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      { comment: '[MASTER] Tenor, total jangka waktu cicilan' }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tenors');
  }
};