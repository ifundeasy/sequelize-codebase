'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'fundTypes',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.TINYINT(4),
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING(100),
          unique: true,
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      {
        comment: '[MASTER] Jenis pinjaman, untuk keperluan apa?',
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('fundTypes');
  }
};