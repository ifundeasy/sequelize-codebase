'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'loanReminders',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        loanId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'loans',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        startDate: {
          allowNull: false,
          type: Sequelize.DATEONLY,
        },
        agentType: {
          allowNull: false,
          type: Sequelize.ENUM(Object.values(Enum.agentType)),
        },
        dueValue: {
          allowNull: false,
          type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
        },
        dueTimeUnit: {
          allowNull: false,
          type: Sequelize.ENUM(Object.keys(Enum.timeUnitType)),
        },
        recurrentValue: {
          type: Sequelize.INTEGER(8).ZEROFILL.UNSIGNED,
        },
        recurrentTimeUnit: {
          type: Sequelize.ENUM(Object.keys(Enum.timeUnitType)),
        },
        inTime: {
          allowNull: false,
          type: Sequelize.TIME,
        },
        isActive: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        createdBy: {
          allowNull: false,
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
        },
        updatedBy: {
          type: Sequelize.STRING(50),
          defaultValue: 'SYSTEM',
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        notes: {
          type: Sequelize.STRING,
          defaultValue: 'NULL',
        },
      },
      {
        comment:
          'Reminder, pengingat jatuh tempo customer, di copy dari tabel reminder ketika ada pinjaman baru sebagai default reminder',
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('loanReminders');
  }
};