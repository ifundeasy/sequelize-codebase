'use strict';

const Enum = require('sequelize-codebase-enum');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('loanStatuses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      loanId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'loans',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      tenorId: {
        type: Sequelize.TINYINT(4),
        references: {
          model: 'tenors',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      installmentId: {
        type: Sequelize.TINYINT(4),
        references: {
          model: 'installments',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      verifyStatus: {
        allowNull: false,
        type: Sequelize.ENUM(Object.values(Enum.verifyStatus)),
      },
      startDate: {
        type: Sequelize.DATEONLY,
      },
      endDate: {
        type: Sequelize.DATEONLY,
      },
      isActive: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdBy: {
        allowNull: false,
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('CURRENT_TIMESTAMP'),
      },
      updatedBy: {
        type: Sequelize.STRING(50),
        defaultValue: 'SYSTEM',
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      notes: {
        type: Sequelize.STRING,
        defaultValue: 'NULL',
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('loanStatuses');
  },
};
