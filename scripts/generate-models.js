const fs = require('fs');
const SequelizeAuto = require('sequelize-auto');
const { execSync } = require('child_process');

const tables = require('../config/tables');
const sequelizerc = require('../.sequelizerc');
const dir = sequelizerc['models-path'];
const tempDir = (dir.substr(-1) === '/' ? dir.substr(0, dir.length - 1) : dir) + '_temp';
const {
  username, password, database, host, port, dialect, 
  migrationStorageTableName, seederStorageTableName
} = require(sequelizerc.config);

const config = {
  host,
  dialect,
  directory: tempDir,
  port,
  caseModel: 'c', // convert snake_case column names to camelCase field names: user_id -> userId
  caseFile: 'c', // file names created for each model use camelCase.js not snake_case.js
  singularize: false,
  additional: {
    timestamps: false,
    paranoid: false,
    underscored: false,
    freezeTableName: true,
  },
  skipTables: [migrationStorageTableName, seederStorageTableName],
};

if (tables.length) config.tables = tables;

const sequelizeAuto = new SequelizeAuto(database, username, password, config);
sequelizeAuto.run().then((data) => {
  // const { table, foreignKeys, indexes, hasTriggerTables, relations, text } = data;
  // console.log({ table, foreignKeys, indexes, hasTriggerTables, relations, text });
  try {
    fs.rmdirSync(dir, { recursive: true });
    fs.renameSync(tempDir, dir);

    const formating = execSync(`npx prettier --write ${dir}`);
    console.log(formating.toString());
  } catch (e) {
    console.error(e);
  }
});