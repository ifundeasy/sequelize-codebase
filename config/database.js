require('dotenv').config();

const { env } = process;

module.exports = {
  username: env.DB_USERNAME,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
  host: env.DB_HOST,
  port: env.DB_PORT,
  dialect: env.DB_DIALECT,
  pool: {
    max: 5, // default: 5
    min: 0, // default: 0
    idle: 10000, // default: 10000
    acquire: 60000, // default: 60000
  },
  migrationStorageTableName: 'sequelizeMetas',
  seederStorageTableName: 'sequelizeSeeds',
};
