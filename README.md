# sequelize-codebase

## Installation

```sh
npm i
```

## Bootstrap Data

```sh
npm run db:createdb
npm run db:migrate
npm run db:seed
```

## Generate Models

```sh
npm run db:model
```
